package droid.cicd.appiumapp;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.appium.java_client.android.AndroidDriver;

public class ApplicationLaunchTest {

	private static AndroidDriver driver;

	@BeforeClass
	public void setup() {

		// File app = new
		// File("D:\\cicd\\AndroidProject\\CICDAppliaction\\dev\\app\\build\\outputs\\apk\\app-debug.apk");

		DesiredCapabilities localCapabilities = new DesiredCapabilities();
		String localURL = "http://127.0.0.1:4723/wd/hub";

		localCapabilities.setCapability(CapabilityType.BROWSER_NAME, "");
		localCapabilities.setCapability("deviceName", "TA64302KK8");
		localCapabilities.setCapability("platformVersion", "5.1");
		localCapabilities.setCapability("platformName", "Android");
		// localCapabilities.setCapability("app", app.getAbsolutePath());
		localCapabilities.setCapability("appPackage", "droid.cicd.cicdappliaction");
		localCapabilities.setCapability("appActivity", "droid.cicd.cicdappliaction.Home");
		localCapabilities.setCapability("selenium.port", "4723");

		//DesiredCapabilities remoteCapabilities = new DesiredCapabilities();
		//String remoteURL = "http://partners.perfectomobile.com/nexperience/perfectomobile/wd/hub";

		//remoteCapabilities.setCapability("user", "is_user2@infostretch.com");
		//remoteCapabilities.setCapability("password", "Infostretch1");
		//remoteCapabilities.setCapability("deviceName", "1DAA7935");
		//remoteCapabilities.setCapability("automationName", "Appium");
		//remoteCapabilities.setCapability("platformName", "Android");
		//remoteCapabilities.setCapability("appPackage", "droid.cicd.cicdappliaction");
		//remoteCapabilities.setCapability("appActivity", "droid.cicd.cicdappliaction.Home");
		//remoteCapabilities.setCapability("selenium.port", "4723");

		try {

			driver = new AndroidDriver(new URL(localURL), localCapabilities);
			
			Hashtable<String, Object> params = new Hashtable<String, Object>();
			params.put("name", "CICDAppliaction");
			params.put("timeout", 20);
			
			String resultStr = (String) driver.executeScript("mobile:application:open", params);
			
			System.out.println("start app result : " + resultStr);
			
			driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
			
			Thread.sleep(10000);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	@AfterClass
	public void tearDown() {
		driver.quit();
	}

	@Test
	public void testLaunch() {
		System.out.println("Application launched successfully");
	}

	@Test
	public void testActionButton() {
		By button = By.id("droid.cicd.cicdappliaction:id/fab");
		driver.findElement(button).click();
	}

	public void testTabs() {
		By tab = By.id("");
		driver.findElement(tab).click();
		tab = By.id("");
		driver.findElement(tab).click();
	}

}
