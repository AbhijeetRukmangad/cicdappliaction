package droid.cicd.cicdappliaction;

import org.junit.Test;

import java.util.Objects;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void verify_number() throws Exception {
        assertEquals(4, 2 + 2);
    }


    @Test
    public void verify_object() throws Exception {
        Object obj = new Object();
        assertEquals(obj,obj);
    }

    @Test
    public void verify_string() throws Exception {
        assertEquals("A","A");
    }
}