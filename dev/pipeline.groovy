   stage 'BuildSource'
   echo 'Building the source code'
   bat "gradlew assemble"
   
   stage 'Analyze'
   echo 'Running static analyzers'
   step([$class: 'CheckStylePublisher', canComputeNew: false, defaultEncoding: '', healthy: '', pattern: '**/Reports/checkstyle_results.xml', unHealthy: ''])
   step([$class: 'FindBugsPublisher', canComputeNew: false, defaultEncoding: '', excludePattern: '', healthy: '', includePattern: '', pattern: '**/Reports/findbugs_results.xml', unHealthy: ''])
   
   stage 'UnitTest'
   echo 'Executing unit test cases'
   bat 'gradlew test'
   
   stage 'Regression'
   echo ' Execute functional/regression automation test cases'
   
   stage 'Deploy'
   echo 'Deploy application'   
   bat 'adb install -d -r app/build/outputs/apk/app-debug.apk'